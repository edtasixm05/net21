# compose / portainer / visualizer
## @edt ASIX M05 Curs 2021-2022

### Descripció


**portainer** Portainer és una imatge docker que permet l'administració de tot l'entorn
de desenvolupament docker.

**visualizer** Es tracta d'una imatge docker que permet mnitorar el desplegament de container,
especialment útil en l'utilització de múltiples nodes amb *swarm*.


**docker-compose** És la tecnologia de docker per desplegar aplicacions compostes de varis dels 
seus components: containers, xarxxes, volums, etc.


## Portainer

Amb *Docker Portainer* podem administrar tot l'entorn de docker.

```
docker create mynet
docker run --rm -p 9000:9000 --net mynet -v /var/run/docker.sock:/var/run/docker.sock -d portainer/portainer

$ docker ps
CONTAINER ID   IMAGE                 COMMAND        CREATED          STATUS          PORTS                                       NAMES
641aed0faeeb   portainer/portainer   "/portainer"   41 seconds ago   Up 40 seconds   0.0.0.0:9000->9000/tcp, :::9000->9000/tcp   exciting_euler
```

Observem que el container s'engegarà amb un nom dinàmic (no n'hi hem assignat cap) i escoltarà per el port 9000. 
El volum que s'hi indica serveix perquè des del container es pugui administrar el propi *Docker Engine*, el motor
de docker.

Per accedir via web al servei que ofereix Portainer cal accedir amb un navegador al **port 9000**, com que
s'ha fet la redirecció de port tant es pot accedir al container com al host amfitrió.

```
Per visualitzar el Portainer cal accedir des d'un navegador a la'dreça localhost:9000

User: admim
Paswwd: el primer cop que s'hi accedeix cal establir el passwd (atenció a la longitud mínima requerida)

Seleccionar "local" per administrar el Docker
```

Mentre el container està en funcionament s'hi accedeix amb l'usuari (admin) i el password creat, si finalitza l'execució
del container la propera vegada que s'engegi caldrà tornar a definir el passwd.

Podem administrar els següents elements:
 * Imatges
 * Containers
 * Volums
 * Networks
 * Stacks


**Aturar el Portainer**

Com que s'ha generat sense indicar un nom, tindrà un nom dinàmic. Per tancar-lo podem fer:
```
$ docker ps
CONTAINER ID   IMAGE                 COMMAND        CREATED         STATUS         PORTS                                       NAMES
641aed0faeeb   portainer/portainer   "/portainer"   9 minutes ago   Up 9 minutes   0.0.0.0:9000->9000/tcp, :::9000->9000/tcp   exciting_euler

$ docker stop exciting_euler 
exciting_euler
```


## Visualizer

Visualizer és una eina que permet visualitzar els desplegaments de containers en varis nodes. És especialment útil en
desplegaments de *Docker Swarm*. Permet veure quins serveis s'estan executant en quins nodes.

A hores d'ara estem fent els exercicis en un únic node, el nostre host amfitrió. Però perquè funcioni caldrà activar
swarm almenys per fer aquesta prova, tot i que en acabar el desactivarem.

**Atenció!** *Aquest exercici només permet veure engegat visualizer però no veure'n la aplicació real, per fer-ho
caldria engegar serveis (no containers) amb les ordres de swarm o compose apropiades. Engegant simplement containers
no es visualitzaran.*
 

```
$ docker swarm init

# Si falla indicar la vostra adreça IP en el advertise 
# docker swarm --advertise-addr 192.168.1.105 

$ docker run --rm -p 8080:8080 -v /var/run/docker.sock:/var/run/docker.sock  -d dockersamples/visualizer:stable
8d0e49b27eeff8d095509b52cd4ec6d568f2b8995855aaa3e88550d33cdfd420

$ docker ps
CONTAINER ID   IMAGE                             COMMAND       CREATED          STATUS          PORTS                                       NAMES
8d0e49b27eef   dockersamples/visualizer:stable   "npm start"   31 seconds ago   Up 28 seconds   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp   wonderful_swanson
```

Aquest container proporciona una interfície gràfica a través del **port 8080** tant del container com del host amfritrió, ja que
s'ha propagat el port.

```
Des d'un navegador accedir a l'adreça localhost:8080
```

**Tancar el container i també swarm**

Primerament aturarem el container *visualizer* i després abandonarem el mode *swarm*.

```
$ docker stop net.edt.org 
net.edt.org

$ docker swarm leave --force
Node left the swarm.
```

## Scripts per engegar múltiples containers

Ens podem fabricar scripts per engegar múltiples containers que tinguem interès en engegar junts, però veurem que aquesta
**no** és la manera apropiada de fer-ho, sinó utilitzant la tecnologia proporcionada per **docker-compose**.

```
$ cat script-net-portainer.sh 
#! /bin/bash
# @edt ASIX-M05 Curs 2021-2022
#
# Engegar els conatiners net21:base i portainer

docker run --rm --name net.edt.org -h net.edt.org --net mynet -p 7:7 -p 13:13 -p 80:80 -d edtasixm05/net21:base
docker run --rm -p 9000:9000 --net mynet -v /var/run/docker.sock:/var/run/docker.sock -d portainer/portainer
docker ps

$ bash script-net-portainer.sh 
c7de833fa996e98a12c536b20ac546c43e2fc23db5554a771e99d48b2448baa0
e1315b1d2e7850f592b3326e0f1f2dff2a01b0dc21c7c324bc6eadc8365eb24e
CONTAINER ID   IMAGE                   COMMAND                  CREATED                  STATUS                  PORTS                                                                                                         NAMES
e1315b1d2e78   portainer/portainer     "/portainer"             Less than a second ago   Up Less than a second   0.0.0.0:9000->9000/tcp, :::9000->9000/tcp                                                                     interesting_colden
c7de833fa996   edtasixm05/net21:base   "/usr/sbin/xinetd -d…"   Less than a second ago   Up Less than a second   0.0.0.0:7->7/tcp, :::7->7/tcp, 0.0.0.0:13->13/tcp, :::13->13/tcp, 0.0.0.0:80->80/tcp, :::80->80/tcp, 19/tcp   net.edt.org
```

## Docker Compose

Docker compose és la tecnologia de docker que permet desplegar aplicacions basades en containers. Utilitza fitxers
en format *YAML* i existeixen varies versions del format del document. Es recomana utilitzar la documentació de referència
de *Docker Documentation*.

En aquest exercici no es pretén explicar i treballar amb docker-compose, que és totta una àrea de docker per si mateixa, 
sinó presentar-ne el model per anar avançant en el mapa de coneixement de totes aquestes tecnologies.


**Instal·lar Docker Compose**


Podeu consultar la documentació de *Doccker dcoumentation* a l'apartat [Install Docker Compose](https://docs.docker.com/compose/install/)

```
#$ dnf -y install docker compose
$ apt-get update
$ apt-get -y install docker-compose
```

```
$ docker-compose up -d
Creating network "compose-portainer-visualizer_mynet" with the default driver
Creating net.edt.org                               ... done

$ docker-compose ps
                  Name                               Command             State                    Ports                  
-------------------------------------------------------------------------------------------------------------------------
compose-portainer-visualizer_visualizer_1   npm start                    Up      0.0.0.0:8080->8080/tcp,:::8080->8080/tcp
net.edt.org                                 /usr/sbin/xinetd -dontfork   Up      13/tcp, 19/tcp, 7/tcp                   
```
```
$ docker-compose down 
Stopping compose-portainer-visualizer_visualizer_1 ... done
Stopping net.edt.org                               ... done
Removing compose-portainer-visualizer_visualizer_1 ... done
Removing net.edt.org                               ... done
Removing network compose-portainer-visualizer_mynet
```

