#! /bin/bash

# Crear usuaris
for user in pere marta anna pau jordi julia
do
    useradd -m $user
    echo $user:$user | chpasswd
done

# Inicialitzar documents d'exemple
echo "@edt ASIX-M05 Curs 2021-2022"  > /var/www/html/index.html
echo "Benvinguts al vsftpd" > /var/ftp/hola.pdf
echo "directori public" > /var/ftp/pub/info.txt
echo "Benvingutts al servei tftp" > /var/lib/tftpboot/hola.txt
echo "llista de fitxers del tftp" > /var/lib/tftpboot/llista.txt
/usr/bin/ssh-keygen -A
touch /var/run/tftp.sock
cp /opt/docker/xinetd.d/* /etc/xinetd.d/
