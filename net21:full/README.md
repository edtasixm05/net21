# net21:full
## @edt ASIX M05 Curs 2021-2022

### Descripció

**edtasixm05/net21:full** Servidor de serveis de xarxa, usant xinetd, redireccions, httpd, 
  vsftp, ftpd, etc. S'utilitza per fer tests de connectivitat, xinetd, tcpwrappers, 
  túnels ssh i iptables.

 * serveis xinetd: echo(7), daytime(13) i chargen(19) stream (tcp).
 * serveis xinetd: echo(7), daytime(13) i chargen(19) dgram (udp).
 * serveis del paquet uw-imap: ipop3(110), imap(143), pop3s (995), imaps (993).
 * servei web httpd(80).
 * serveis xinetd redireccions: 2007 (7), 2013 (13) i 2080 (80). També 3080 (www.gmail.com 80)
 * serveis xinetd redireccions bound al loopback: 4007 (7), 4013 (13), 4080 (80).
 * servei vsftpd(20,21).
 * servei sshd(22).
 * servei tftp (69).

 
Test
```
# telnet a.b.c.d port
# netstat -tan
# netstat -uan
# nmap localhost
# nmap a.b.c.d
```

Execució:

Sense mapejar els ports
```
docker netweork create mynet
docker run --rm --name nethost -h nethost --net mynet -d edtasixm05/net21:full
```

Amb ports mapejats
```
docker netweork create mynet
docker run --rm --name nethost -h nethost --net mynet -p 7:7 -p 13:13 -p 80:80 -d edtasixm05/net21:full
```

#### Exemple de funcionament

```
$ docker network create mynet
9e81d292892bf03f4dd109f8cc0949e695b1e653700bcd3c2d0c7376533185cf

$ docker run --rm --name nethost -h nethost --net mynet -p 7:7 -p 13:13 -p 80:80 -d edtasixm05/net21:full
f6a8b32457ebe66decb68a662aec4a5dff65dd76149d808066f91bf5be78dc1c

$ docker ps
CONTAINER ID   IMAGE                   COMMAND                  CREATED         STATUS         PORTS                                                                                                 NAMES
f6a8b32457eb   edtasixm05/net21:full   "/opt/docker/startup…"   5 seconds ago   Up 4 seconds   0.0.0.0:7->7/tcp, :::7->7/tcp, 0.0.0.0:13->13/tcp, :::13->13/tcp, 0.0.0.0:80->80/tcp, :::80->80/tcp   nethost
```

```
$ docker network inspect mynet
[
    {
        "Name": "mynet",
        "Id": "9e81d292892bf03f4dd109f8cc0949e695b1e653700bcd3c2d0c7376533185cf",
        "Created": "2022-05-06T10:27:55.176253372+02:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.23.0.0/16",
                    "Gateway": "172.23.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "f6a8b32457ebe66decb68a662aec4a5dff65dd76149d808066f91bf5be78dc1c": {
                "Name": "nethost",
                "EndpointID": "a75b4c563d194c400da3c17e45830e622720f85f2c46b3da1a2e3c12f33aa579",
                "MacAddress": "02:42:ac:17:00:02",
                "IPv4Address": "172.23.0.2/16",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {}
    }
]
```

Llistat de ports oberts en el container nethost
```
$ nmap 172.23.0.2
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-06 10:30 CEST
Nmap scan report for 172.23.0.2
Host is up (0.000080s latency).
Not shown: 984 closed ports
PORT     STATE SERVICE
7/tcp    open  echo
13/tcp   open  daytime
19/tcp   open  chargen
21/tcp   open  ftp
22/tcp   open  ssh
37/tcp   open  time
80/tcp   open  http
110/tcp  open  pop3
143/tcp  open  imap
993/tcp  open  imaps
995/tcp  open  pop3s
2013/tcp open  raid-am
2022/tcp open  down
3013/tcp open  gilatskysurfer
5080/tcp open  onscreen
8080/tcp open  http-proxy
```

## Test Client / Servidor  amb dos containers


Anem a veure com usant Docker podem fer proves de serveis *Client / Server* sense la necessitat 
d'utilitzar els típics desplegaments amb màquines virtuals. Es desplega un container amb el servidor
i com a client es pot usar el propri host amfitriò o un (o més) containers docker clients.

Aquests containers clients poden ser simples desplegaments de container de Debian, Fedora, Ubuntu, etc, on,
si cal, s'hi instal·la el software client. 

Network:

 * Per poder fer que els conatiner ses comuniquin entre ells per nom de container en lloc d'adreça IP
   cal que comparteixin una xarxa creada específicament amb docker network. 

 * És recomanable que es facin servir *FQDN Fully Qualified Domain Names*,
   perquè en situacions d'implementació reals els serveis tendeixen a implementar aquest requeriment. Si ens 
   hi acostumem de bon principi millor.

```
$ docker run --rm --name net.edt.org -h net.edt.org --net mynet -p 7:7 -p 13:13 -p 80:80 -d edtasixm05/net21:full
bff6b8bc68f08b8e940c4be75d423bee73d75e1609c644946f2c763f4efc8ed7
```

```
$ docker run --rm --name host.edt.org -h host.edt.org --net mynet -it debian /bin/bash

# apt-get update && apt-get install -y iproute2 iputils-ping nmap

root@host:/# ping -c2 net.edt.org
PING net.edt.org (172.23.0.2) 56(84) bytes of data.
64 bytes from net.edt.org.mynet (172.23.0.2): icmp_seq=1 ttl=64 time=0.206 ms
64 bytes from net.edt.org.mynet (172.23.0.2): icmp_seq=2 ttl=64 time=0.101 ms
2 packets transmitted, 2 received, 0% packet loss, time 1001ms

root@host:/# ping -c2 host.edt.org
PING host.edt.org (172.23.0.3) 56(84) bytes of data.
64 bytes from host.edt.org (172.23.0.3): icmp_seq=1 ttl=64 time=0.079 ms
64 bytes from host.edt.org (172.23.0.3): icmp_seq=2 ttl=64 time=0.053 ms
2 packets transmitted, 2 received, 0% packet loss, time 1007ms

root@host:/# nmap net.edt.org
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-06 09:29 UTC
Nmap scan report for net.edt.org (172.23.0.2)
Host is up (0.000015s latency).
rDNS record for 172.23.0.2: net.edt.org.mynet
Not shown: 984 closed ports
PORT     STATE SERVICE
7/tcp    open  echo
13/tcp   open  daytime
19/tcp   open  chargen
21/tcp   open  ftp
22/tcp   open  ssh
37/tcp   open  time
80/tcp   open  http
110/tcp  open  pop3
143/tcp  open  imap
993/tcp  open  imaps
995/tcp  open  pop3s
2013/tcp open  raid-am
2022/tcp open  down
3013/tcp open  gilatskysurfer
5080/tcp open  onscreen
8080/tcp open  http-proxy
```

**Crear un client personalitzat**

Generalment per fer proves de funcionment d'un servidor utilitzarem un client personalitzat que 
tingui les eines necessaries per poder fer les proves que volem fer. En l'exemple anterior s'ha 
instal·lat nmap i ping a un client debian. Per no fer-ho cada vegada és més pràctic crear 
directament una imatge client que inclogui ja les eines.

En l'exemple del servidor *net* podem crear una client amb les eines clients que colem utilitzar
per provar els serveis de xarxa.

Servidor net.edt.org
```
$ docker run --rm --name net.edt.org -h net.edt.org --net mynet -p 7:7 -p 13:13 -p 80:80 -d edtasixm05/net21:full

$ docker ps
CONTAINER ID   IMAGE                   COMMAND                  CREATED          STATUS          PORTS                                                                                                 NAMES
bff6b8bc68f0   edtasixm05/net21:full   "/opt/docker/startup…"   21 minutes ago   Up 21 minutes   0.0.0.0:7->7/tcp, :::7->7/tcp, 0.0.0.0:13->13/tcp, :::13->13/tcp, 0.0.0.0:80->80/tcp, :::80->80/tcp   net.edt.org
``` 

Client per fer test al servidor
```
$ docker run --rm --name host.edt.org -h host.edt.org --net mynet -it  edtasixm05/net21:client

[root@host tmp]# telnet net.edt.org 13
Trying 172.23.0.2...
Connected to net.edt.org.
Escape character is '^]'.
06 MAY 2022 09:56:28 UTC
Connection closed by foreign host.


```



#### Exemples de desplegaments Client/Servidor

Igual que s'ha fet en aquest exemple engegant un servidor de serveis de xarxa *net.edt.org* i un client 
per fer-ne proves de connectivitat (ha calgut instal·lar-hi les eines client), podem fer-ho amb més
clients i emb altres serveis  tipus Client/Server amb els que volguem practicar, per exemple:

 * Servidor Ldap / Clients per fer consultes ldap
 * Servidor Ldap / Clients d'autenticació contra ldap
 * Servidor Kerberos  / Clients per fer consultes a la base de  dades kerberos
 * Servidor Kerberos / Clients d'autenticació d'usuairs amb Ldap + Kerberos + PAM
 * Servidor NFS / Client NFS que munta recursos de disc del servei NFS
 * Servidor SAMBA / Client Samba que munta recursos del servidor Samba
 * Servidor SSH / Client SSH que utilitza recursos del servidor SSH
 * Servidors Ldap + Kerberos + Homes (SSH o NFS o Samba) / Client PAM que autentica i munta home dels usuaris

