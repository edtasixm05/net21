# net21:client
## @edt ASIX M05 Curs 2021-2022

### Descripció

**edtasixm05/net21:client** Client de serveis de xarxa per usar per provar el funcionament
dels serveis que ofereix el servidor *edtasixm05/net21:full*. Inclou els programes client
telnet, wget, ftp, tftp, etc.



Engegar el servidor

```
$ docker network create mynet
9e81d292892bf03f4dd109f8cc0949e695b1e653700bcd3c2d0c7376533185cf

$ docker run --rm --name nethost -h nethost --net mynet -p 7:7 -p 13:13 -p 80:80 -d edtasixm05/net21:full
f6a8b32457ebe66decb68a662aec4a5dff65dd76149d808066f91bf5be78dc1c

$ docker ps
CONTAINER ID   IMAGE                   COMMAND                  CREATED         STATUS         PORTS                                                                                                 NAMES
f6a8b32457eb   edtasixm05/net21:full   "/opt/docker/startup…"   5 seconds ago   Up 4 seconds   0.0.0.0:7->7/tcp, :::7->7/tcp, 0.0.0.0:13->13/tcp, :::13->13/tcp, 0.0.0.0:80->80/tcp, :::80->80/tcp   nethost
```

Client per fer test
```
$ docker run --rm --name host.edt.org -h host.edt.org --net mynet -it  edtasixm05/net21:client
```

#### Serveis xinetd daytime(13), echo (7) i chargen(19)
```
[root@host tmp]# telnet net.edt.org 13
Trying 172.23.0.2...
Connected to net.edt.org.
Escape character is '^]'.
06 MAY 2022 09:56:28 UTC
Connection closed by foreign host.

[root@host tmp]# telnet net.edt.org 7  
Trying 172.18.0.2...
Connected to net.edt.org.
Escape character is '^]'.
hola
hola
això és un echo server
això és un echo server
^]
telnet> quit
Connection closed.

[root@host tmp]# telnet net.edt.org 19
...
bcdefghijklmnopqrstuvwxyz{|}!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKL
cdefghijklmnopqrstuvwxyz{|}!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLM
defghijklmnopqrstuvwxyz{|}!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMN
efghijklmnopqrstuvwxyz{|}!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNO
fghijklmnopqrstuvwxyz{|}!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOP
...
telnet> quit      
Connection closed.
```


#### Serveis POP3 i IMAP
```
[root@host tmp]# telnet net.edt.org 110
Trying 172.18.0.2...
Connected to net.edt.org.
Escape character is '^]'.
+OK POP3 net.edt.org 2007f.104 server ready
USER pere
+OK User name accepted, password please
PASS pere
+OK Mailbox open, 0 messages
STAT
+OK 0 0
LIST
+OK Mailbox scan listing follows
.
quit
+OK Sayonara
Connection closed by foreign host.

[root@host tmp]# telnet net.edt.org 143
Trying 172.18.0.2...
Connected to net.edt.org.
Escape character is '^]'.
* OK [CAPABILITY IMAP4REV1 I18NLEVEL=1 LITERAL+ SASL-IR LOGIN-REFERRALS STARTTLS] net.edt.org IMAP4rev1 2007f.404 at Fri, 6 May 2022 15:24:20 +0000 (UTC)
a001 login marta marta
a001 OK [CAPABILITY IMAP4REV1 I18NLEVEL=1 LITERAL+ IDLE UIDPLUS NAMESPACE CHILDREN MAILBOX-REFERRALS BINARY UNSELECT ESEARCH WITHIN SCAN SORT THREAD=REFERENCES THREAD=ORDEREDSUBJECT MULTIAPPEND] User marta authenticated
a002 select inbox
* 0 EXISTS
* 0 RECENT
* OK [UIDVALIDITY 1651850709] UID validity status
* OK [UIDNEXT 1] Predicted next UID
* FLAGS (\Answered \Flagged \Deleted \Draft \Seen)
* OK [PERMANENTFLAGS ()] Permanent flags
a002 OK [READ-WRITE] SELECT completed
a003 logout
* BYE net.edt.org IMAP4rev1 server terminating connection
a003 OK LOGOUT completed
Connection closed by foreign host.
```

##### Servei web
```
[root@host tmp]# telnet net.edt.org 80
Trying 172.18.0.2...
Connected to net.edt.org.
Escape character is '^]'.
GET / HTTP/1.0

HTTP/1.1 200 OK
Date: Fri, 06 May 2022 15:30:19 GMT
Server: Apache/2.4.34 (Fedora)
Last-Modified: Fri, 06 May 2022 15:14:54 GMT
ETag: "12-5de595217b769"
Accept-Ranges: bytes
Content-Length: 18
Connection: close
Content-Type: text/html; charset=UTF-8

@edt ASIX-M05 Curs 2021-2022
Connection closed by foreign host.
```

#### Servei SSH
```
root@host tmp]# ssh pere@net.edt.org
The authenticity of host 'net.edt.org (172.18.0.2)' can't be established.
ECDSA key fingerprint is SHA256:URmn3fWGv/teT9LEaX0lO/IG2siJDww3kusxlZ3Mi/0.
ECDSA key fingerprint is MD5:a3:f1:57:b8:49:0f:27:57:37:31:86:35:91:f2:78:a0.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'net.edt.org,172.18.0.2' (ECDSA) to the list of known hosts.
pere@net.edt.org's password: 
[pere@net ~]$ id
uid=1000(pere) gid=1000(pere) groups=1000(pere)
[pere@net ~]$ exit
logout
Connection to net.edt.org closed.
```


#### Servei FTP
```
[root@host tmp]# ftp net.edt.org
Connected to net.edt.org (172.18.0.2).
220 (vsFTPd 3.0.3)
Name (net.edt.org:root): anonymous
331 Please specify the password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls
227 Entering Passive Mode (172,18,0,2,76,167).
150 Here comes the directory listing.
-rw-r--r--    1 0        0              21 May 06 15:31 hola.pdf
drwxr-xr-x    1 0        0            4096 May 06 15:31 pub
226 Directory send OK.
ftp> quit
221 Goodbye.
```

#### Servei TFTP
```
[root@host tmp]# tftp net.edt.org
tftp> verbose
Verbose mode on.
tftp> trace
Packet tracing on.
tftp> get hola.txt
getting from net.edt.org:hola.txt to hola.txt [netascii]
sent RRQ <file=hola.txt, mode=netascii>
received DATA <block=1, 28 bytes>
Received 28 bytes in 0.0 seconds [75446 bit/s]
tftp> quit
```

#### Redireccions als ports 2007(7), 2013(13), 2080(80) i 3080(www.gmail.com)  
```
[root@host tmp]# telnet net.edt.org 2007
Trying 172.18.0.2...
Connected to net.edt.org.
Escape character is '^]'.
servidor
servidor
echo
echo
^]
telnet> quit
Connection closed.

[root@host tmp]# telnet net.edt.org 2013
Trying 172.18.0.2...
Connected to net.edt.org.
Escape character is '^]'.
06 MAY 2022 16:03:01 UTC
Connection closed by foreign host.

[root@host tmp]# telnet net.edt.org 2080
Trying 172.18.0.2...
Connected to net.edt.org.
Escape character is '^]'.
GET / HTTP/1.0

HTTP/1.1 200 OK
Date: Fri, 06 May 2022 15:51:18 GMT
Server: Apache/2.4.34 (Fedora)
Last-Modified: Fri, 06 May 2022 15:44:06 GMT
ETag: "1d-5de59ba882f45"
Accept-Ranges: bytes
Content-Length: 29
Connection: close
Content-Type: text/html; charset=UTF-8

@edt ASIX-M05 Curs 2021-2022
Connection closed by foreign host.


[root@host tmp]# telnet net.edt.org 3080
Trying 172.18.0.2...
Connected to net.edt.org.
Escape character is '^]'.
GET / HTTP/1.0
...
```

#### Serveis POP3s i IMAPs (segurs)
```
[root@host tmp]# openssl s_client -connect net.edt.org 995
s_client: Use -help for summary.
[root@host tmp]# openssl s_client -connect net.edt.org:995
CONNECTED(00000003)
depth=0 C = --, ST = SomeState, L = SomeCity, O = SomeOrganization, OU = SomeOrganizationalUnit, CN = localhost.localdomain, emailAddress = root@localhost.localdomain
verify error:num=18:self signed certificate
verify return:1
depth=0 C = --, ST = SomeState, L = SomeCity, O = SomeOrganization, OU = SomeOrganizationalUnit, CN = localhost.localdomain, emailAddress = root@localhost.localdomain
verify return:1
---
Certificate chain
 0 s:/C=--/ST=SomeState/L=SomeCity/O=SomeOrganization/OU=SomeOrganizationalUnit/CN=localhost.localdomain/emailAddress=root@localhost.localdomain
   i:/C=--/ST=SomeState/L=SomeCity/O=SomeOrganization/OU=SomeOrganizationalUnit/CN=localhost.localdomain/emailAddress=root@localhost.localdomain
---
Server certificate
-----BEGIN CERTIFICATE-----
MIIETjCCAzagAwIBAgIJANdWURxFICz5MA0GCSqGSIb3DQEBCwUAMIG7MQswCQYD
VQQGEwItLTESMBAGA1UECAwJU29tZVN0YXRlMREwDwYDVQQHDAhTb21lQ2l0eTEZ
MBcGA1UECgwQU29tZU9yZ2FuaXphdGlvbjEfMB0GA1UECwwWU29tZU9yZ2FuaXph
dGlvbmFsVW5pdDEeMBwGA1UEAwwVbG9jYWxob3N0LmxvY2FsZG9tYWluMSkwJwYJ
KoZIhvcNAQkBFhpyb290QGxvY2FsaG9zdC5sb2NhbGRvbWFpbjAeFw0yMjA1MDYx
NTE0MDBaFw0yMzA1MDYxNTE0MDBaMIG7MQswCQYDVQQGEwItLTESMBAGA1UECAwJ
U29tZVN0YXRlMREwDwYDVQQHDAhTb21lQ2l0eTEZMBcGA1UECgwQU29tZU9yZ2Fu
aXphdGlvbjEfMB0GA1UECwwWU29tZU9yZ2FuaXphdGlvbmFsVW5pdDEeMBwGA1UE
AwwVbG9jYWxob3N0LmxvY2FsZG9tYWluMSkwJwYJKoZIhvcNAQkBFhpyb290QGxv
Y2FsaG9zdC5sb2NhbGRvbWFpbjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
ggEBAKZivpPF7VAaqKYjjlscgn5UP8fVW0stb6p+OOQFDbXJ4IyB6uov42XiDQXG
UhqCznU+wTMC8GPy2LXKc5cqBHD3XOvAxA26XSEJamb11InJ/zqyfJLQ4JrB5bss
+n46Ou471YrFtJdfBYsKCMg3GSQskR+mQT6c5qfRiDSx7/7wwkPa0awW12V0y4p4
Xys++lAcqoEN3K3bgQa9gEsO8wMRav854kONzOuvtdWesRYjYq+4HH8gYmAqfvT+
Z5esoXCz5+jGUsW1fareFY3CK+vWmGfXA2BKSLRHWX6B7p9W91iPTfYZbfW1VzgN
9kkhwpVkW2WaWDKs2ithll4NNLECAwEAAaNTMFEwHQYDVR0OBBYEFFdKifKUFlo/
WDtOU4LN7bDsrQGvMB8GA1UdIwQYMBaAFFdKifKUFlo/WDtOU4LN7bDsrQGvMA8G
A1UdEwEB/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAFMQ7LiGyUrgRvrWaznN
iXqiuo5MwRZWjbWQpC99+37jN3gYVInq0ABcn7t1iPOn2b/RU6XL18YG2IL850VR
e6Gv6SNSybhSlRXj+5JFtpiSSq4Y85IZPQwK55ia/1Mei3jTYfFWnkmMCaseXGle
/Y33wz2EJEAAWHj4D7Lt7lXkkc6uubmqybT0xdBdwyrG9KkArRBEwfwp6BeZQGc9
bOXtfNSw7cuC6me2BW/7917mlS19NUOFecvuxPO4OWCFpxV+6oLd+xkIUa4ssLdg
ix8mLEk925Vr0W0QPAyuu0/R7bDlYUi73bk+q1VRiWsEL6UrdkG9BcxWX7qAX/Kk
imA=
-----END CERTIFICATE-----
subject=/C=--/ST=SomeState/L=SomeCity/O=SomeOrganization/OU=SomeOrganizationalUnit/CN=localhost.localdomain/emailAddress=root@localhost.localdomain
issuer=/C=--/ST=SomeState/L=SomeCity/O=SomeOrganization/OU=SomeOrganizationalUnit/CN=localhost.localdomain/emailAddress=root@localhost.localdomain
---
No client certificate CA names sent
Peer signing digest: SHA512
Server Temp Key: X25519, 253 bits
---
SSL handshake has read 1731 bytes and written 347 bytes
Verification error: self signed certificate
---
New, TLSv1.2, Cipher is ECDHE-RSA-AES256-GCM-SHA384
Server public key is 2048 bit
Secure Renegotiation IS supported
Compression: NONE
Expansion: NONE
No ALPN negotiated
SSL-Session:
    Protocol  : TLSv1.2
    Cipher    : ECDHE-RSA-AES256-GCM-SHA384
    Session-ID: 9DE57F56B533B507D84B56D0048CA8BCFA88110C28FC0F22BB3F391198A9D5C5
    Session-ID-ctx: 
    Master-Key: 98F3FC3AEF5AD410E9B01ADB5A3F072A8203AA5CE20DE8A0E8DC354615FBA499749B537098B9F6AFF9E906A1315BF673
    PSK identity: None
    PSK identity hint: None
    SRP username: None
    TLS session ticket lifetime hint: 7200 (seconds)
    TLS session ticket:
    0000 - d2 57 80 e2 23 51 16 51-7e a0 5a cf 3b c8 30 ce   .W..#Q.Q~.Z.;.0.
    0010 - 68 61 18 db 96 7a 61 71-47 67 bb 27 bc b7 17 02   ha...zaqGg.'....
    0020 - 32 2e ce 16 0f 59 80 1f-1b d7 82 06 12 fc 86 94   2....Y..........
    0030 - a5 5f 1d 52 fe 43 43 43-56 6c c7 d5 ec d5 78 51   ._.R.CCCVl....xQ
    0040 - 16 04 81 df 36 b4 bf af-57 c1 bf 2c 09 87 74 85   ....6...W..,..t.
    0050 - 75 b2 82 49 1d cb b4 fa-10 7b 47 43 7a a2 0f 72   u..I.....{GCz..r
    0060 - 02 3c 48 a2 a6 07 da b9-ba 08 01 a8 e8 07 fc cf   .<H.............
    0070 - fe 75 f1 11 88 5d 77 58-b4 e8 43 59 f3 c7 88 b3   .u...]wX..CY....
    0080 - 11 1f 0b 40 98 b0 5c 80-c0 6c b5 a4 32 73 cb 34   ...@..\..l..2s.4
    0090 - 0e 68 f1 50 d2 f1 88 8e-54 5c 8b e8 8f 05 87 f0   .h.P....T\......

    Start Time: 1651853706
    Timeout   : 7200 (sec)
    Verify return code: 18 (self signed certificate)
    Extended master secret: yes
---
+OK POP3 net.edt.org 2007f.104 server ready
USER anna
+OK User name accepted, password please
PASS anna
+OK Mailbox open, 0 messages
quit
+OK Sayonara
read:errno=0
```
```
[root@host tmp]# openssl s_client -connect net.edt.org:993
CONNECTED(00000003)
depth=0 C = --, ST = SomeState, L = SomeCity, O = SomeOrganization, OU = SomeOrganizationalUnit, CN = localhost.localdomain, emailAddress = root@localhost.localdomain
verify error:num=18:self signed certificate
verify return:1
depth=0 C = --, ST = SomeState, L = SomeCity, O = SomeOrganization, OU = SomeOrganizationalUnit, CN = localhost.localdomain, emailAddress = root@localhost.localdomain
verify return:1
---
Certificate chain
 0 s:/C=--/ST=SomeState/L=SomeCity/O=SomeOrganization/OU=SomeOrganizationalUnit/CN=localhost.localdomain/emailAddress=root@localhost.localdomain
   i:/C=--/ST=SomeState/L=SomeCity/O=SomeOrganization/OU=SomeOrganizationalUnit/CN=localhost.localdomain/emailAddress=root@localhost.localdomain
---
Server certificate
-----BEGIN CERTIFICATE-----
MIIETjCCAzagAwIBAgIJAPeX7bvddj5ZMA0GCSqGSIb3DQEBCwUAMIG7MQswCQYD
VQQGEwItLTESMBAGA1UECAwJU29tZVN0YXRlMREwDwYDVQQHDAhTb21lQ2l0eTEZ
MBcGA1UECgwQU29tZU9yZ2FuaXphdGlvbjEfMB0GA1UECwwWU29tZU9yZ2FuaXph
dGlvbmFsVW5pdDEeMBwGA1UEAwwVbG9jYWxob3N0LmxvY2FsZG9tYWluMSkwJwYJ
KoZIhvcNAQkBFhpyb290QGxvY2FsaG9zdC5sb2NhbGRvbWFpbjAeFw0yMjA1MDYx
NTE0MDBaFw0yMzA1MDYxNTE0MDBaMIG7MQswCQYDVQQGEwItLTESMBAGA1UECAwJ
U29tZVN0YXRlMREwDwYDVQQHDAhTb21lQ2l0eTEZMBcGA1UECgwQU29tZU9yZ2Fu
aXphdGlvbjEfMB0GA1UECwwWU29tZU9yZ2FuaXphdGlvbmFsVW5pdDEeMBwGA1UE
AwwVbG9jYWxob3N0LmxvY2FsZG9tYWluMSkwJwYJKoZIhvcNAQkBFhpyb290QGxv
Y2FsaG9zdC5sb2NhbGRvbWFpbjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
ggEBANnqJC9Sf9w70sfb1l/H90rbwbNhyBrIqy6VFSpHgaoSEBC5JdQlalQp9ixs
/gl8CJj47m95DfxjAhTAYxEW7JYgbiK0/squQ1fWu/X7MxkeOVyn+t9f6TxRvclV
eKgfbE6cfl/H7kVJoYITNoZErhlryflXipkYOqjVL5hhBVpZ18flS6Mk+2Mf42M1
Qsx3d9hx+M9S870WPCi5IAaihehclgl61Vj5qwzqpkSMViJmw5pQdPyHbZziQt/Y
4YE5YDyUECppzYY5U+zjLRRPS8XV3oPtudyZplBSeaDIuHV8TIcOzLkdvTSd35jK
PzF7GPn/5QAK+2vpu2YdWsXWwl0CAwEAAaNTMFEwHQYDVR0OBBYEFLH3qo6yh15/
RklAD5iAAyS/yjpxMB8GA1UdIwQYMBaAFLH3qo6yh15/RklAD5iAAyS/yjpxMA8G
A1UdEwEB/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBACa0f4Gs8KWkNOGDwg4Y
ijdPZ4GRDtZKFIYq3SeI7nqmf5JY9o/quUEqYwe3GGA8XlasZ3QD9PeG5ADFZtCY
r8ABNASEJMkU/QqIzM/Up1dw6EntWpPYVgQZeAGXLTuRQuYbNGni+LiqKVHTByVq
K18u7iT0JPz77vfSYLYmaB6rnTsQD9KKyYFRhmgOt23DgggLJrrpzwPVRYcKq2b/
zXkSe4+CEbrvMt2KE7CVMKTrCWHyrg2dfekgnSrw965+QwDMDcfGOPx4mf6c9rz8
u/Yl1hSPKfgZsyF26v9eDrlKGrXsFzHd/zyNg2hul9PxgRm3HLthBDwobNXmWc3A
Uf8=
-----END CERTIFICATE-----
subject=/C=--/ST=SomeState/L=SomeCity/O=SomeOrganization/OU=SomeOrganizationalUnit/CN=localhost.localdomain/emailAddress=root@localhost.localdomain
issuer=/C=--/ST=SomeState/L=SomeCity/O=SomeOrganization/OU=SomeOrganizationalUnit/CN=localhost.localdomain/emailAddress=root@localhost.localdomain
---
No client certificate CA names sent
Peer signing digest: SHA512
Server Temp Key: X25519, 253 bits
---
SSL handshake has read 1731 bytes and written 347 bytes
Verification error: self signed certificate
---
New, TLSv1.2, Cipher is ECDHE-RSA-AES256-GCM-SHA384
Server public key is 2048 bit
Secure Renegotiation IS supported
Compression: NONE
Expansion: NONE
No ALPN negotiated
SSL-Session:
    Protocol  : TLSv1.2
    Cipher    : ECDHE-RSA-AES256-GCM-SHA384
    Session-ID: C0F0A09244E15AF35774A6DBA1A0412CF3687B000CB8B3C628CF3947C144CF46
    Session-ID-ctx: 
    Master-Key: D9DEE27FF2993FD183A4CF49CE49DBC22B3EB87968AD1C1DE4A6C2B7402003A57D9230AE93312778FD34B35254A28165
    PSK identity: None
    PSK identity hint: None
    SRP username: None
    TLS session ticket lifetime hint: 7200 (seconds)
    TLS session ticket:
    0000 - 90 22 07 51 70 0a 64 55-3a ff 08 dc 6f 32 df 90   .".Qp.dU:...o2..
    0010 - b3 2e e1 74 73 21 8b d8-82 ae fc c6 6d 49 42 5e   ...ts!......mIB^
    0020 - 75 a4 71 aa 8c b8 67 74-4c 2e c8 07 2b 0d 4a c5   u.q...gtL...+.J.
    0030 - 73 b2 9e 6f 0d 3f 52 41-23 60 a3 02 8e 53 e4 24   s..o.?RA#`...S.$
    0040 - b6 6d cd 7f 97 58 2e 77-c0 c5 af 96 c9 0d 7a 91   .m...X.w......z.
    0050 - 07 6c fa 8a fd ff 29 2b-cf a2 53 18 a2 32 55 b1   .l....)+..S..2U.
    0060 - 9f 40 4f 75 e2 56 23 93-27 55 6d 56 a4 2f aa 4a   .@Ou.V#.'UmV./.J
    0070 - 49 2d 9a 9f 25 ab d9 c3-20 b2 19 e8 38 a3 e7 da   I-..%... ...8...
    0080 - a0 87 24 24 61 4e 1e 44-9d 62 25 5e 76 7a bf 4b   ..$$aN.D.b%^vz.K
    0090 - f5 cd 12 f7 4f c8 a7 ab-1a ab 8b c4 15 63 9d f5   ....O........c..

    Start Time: 1651853843
    Timeout   : 7200 (sec)
    Verify return code: 18 (self signed certificate)
    Extended master secret: yes
---
* OK [CAPABILITY IMAP4REV1 I18NLEVEL=1 LITERAL+ SASL-IR LOGIN-REFERRALS AUTH=PLAIN AUTH=LOGIN] net.edt.org IMAP4rev1 2007f.404 at Fri, 6 May 2022 16:17:23 +0000 (UTC)
a001 login julia julia
a001 OK [CAPABILITY IMAP4REV1 I18NLEVEL=1 LITERAL+ IDLE UIDPLUS NAMESPACE CHILDREN MAILBOX-REFERRALS BINARY UNSELECT ESEARCH WITHIN SCAN SORT THREAD=REFERENCES THREAD=ORDEREDSUBJECT MULTIAPPEND] User julia authenticated
a002 logout
* BYE net.edt.org IMAP4rev1 server terminating connection
a002 OK LOGOUT completed
read:errno=0
```

