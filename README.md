# net21
## @edt ASIX-M05 Curs 2021-2022


### Exemples de creació d'imatges de serveis de xarxa

 * **edtasixm05/ssh21:base** Servidor ssh detach.

 * **edtasixm05/samba21:base** Servidor samba detach.

 * **edtasixm05/net21:base** Servidor de serveis de xarxa amb xinetd.

 * **edtasixm05/net21:full** Servidor amb *molts* serveis de xarxa instal·lats.
 
 * **edtasixm05/net21:client** Client amb les eines client per provar els serveis del servidor net21.

