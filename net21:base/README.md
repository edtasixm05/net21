# net21:base
## @edt ASIX M05 Curs 2021-2022

### Descripció

**edtasixm05/net21:full** Servidor de serveis de xarxa, usant xinetd, en concret
els serveis tCP daytime, echo i chargen.

 * serveis xinetd: echo(7), daytime(13) i chargen(19) stream (tcp).
 
Test
```
# telnet a.b.c.d port
# netstat -tan
# netstat -uan
# nmap localhost
# nmap a.b.c.d
```

Execució:

Sense mapejar els ports
```
docker netweork create mynet
docker run --rm --name net.edt.org -h net.edt.org --net mynet -d edtasixm05/net21:base
```

Amb ports mapejats
```
docker netweork create mynet
docker run --rm --name net.edt.org -h net.edt.org --net mynet -p 7:7 -p 13:13 -p 80:80 -d edtasixm05/net21:base
```

#### Exemple de funcionament

```
$ docker network create mynet
9e81d292892bf03f4dd109f8cc0949e695b1e653700bcd3c2d0c7376533185cf

$ docker run --rm --name net.edt.org -h net.edt.org --net mynet -p 7:7 -p 13:13 -p 80:80 -d edtasixm05/net21:base 
be2b66a23dfa74c8212b4949d6973a3e04a29042f638ddc3373f0022afbb4e50

$ docker ps
CONTAINER ID   IMAGE                     COMMAND                  CREATED         STATUS         PORTS                                                                                                         NAMES
be2b66a23dfa   edtasixm05/net21:base     "/usr/sbin/xinetd -d…"   3 seconds ago   Up 2 seconds   0.0.0.0:7->7/tcp, :::7->7/tcp, 0.0.0.0:13->13/tcp, :::13->13/tcp, 0.0.0.0:80->80/tcp, :::80->80/tcp, 19/tcp   net.edt.org
```

Llistat de ports oberts en el container net.edt.org
```
[ecanet@mylaptop net21:base]$ docker exec -it net.edt.org nmap localhost
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-06 16:42 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.0000030s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 997 closed ports
PORT   STATE SERVICE
7/tcp  open  echo
13/tcp open  daytime
19/tcp open  chargen
```

####Client per fer test al servidor

```
$ docker run --rm --name host.edt.org -h host.edt.org --net mynet -it  edtasixm05/net21:client

root@host tmp]# telnet net.edt.org 13
Trying 172.18.0.2...
Connected to net.edt.org.
Escape character is '^]'.
06 MAY 2022 16:44:19 UTC
Connection closed by foreign host.

[root@host tmp]# telnet net.edt.org 7 
Trying 172.18.0.2...
Connected to net.edt.org.
Escape character is '^]'.
hola
hola
sóc un echo server
sóc un echo server
^]
telnet> quit
Connection closed.

[root@host tmp]# telnet net.edt.org 19
abcdefghijklmnopqrstuvwxyz{|}!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJK
bcdefghijklmnopqrstuvwxyz{|}!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKL
cdefghijklmnopqrstuvwxyz{|}!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLM
defghijklmnopqrstuvwxyz{|}!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMN
...
telnet> quit
Connection closed.
```


